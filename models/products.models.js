const mongoose = require('mongoose');

const productsSchema = mongoose.Schema({
  name: String,
  thumbnail: String,
  price: String,
  detail: String
});

const Product = mongoose.model("products", productsSchema);
module.exports = Product;