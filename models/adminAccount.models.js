const mongoose = require('mongoose');

const adminSchema = mongoose.Schema({
  username: String,
  password: String
});

const AdminAccount = mongoose.model("admin", adminSchema);
module.exports = AdminAccount;