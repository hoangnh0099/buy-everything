const mongoose = require('mongoose');
require('dotenv').config();
mongoose.set('useFindAndModify', false);

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true });