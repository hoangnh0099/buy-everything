const AdminAccount = require('../models/adminAccount.models');

module.exports.authLogin = function(req, res, next) {
  if (!req.signedCookies.adminId) {
    res.redirect('/admin');
  }

  if (req.signedCookies.adminId !== '5d48fad037fc294e18fc6b10') {
    res.redirect('/admin');
  }

  next();
}