const express = require('express');
const router = express.Router();

const indexController = require('./../controllers/index.controller');
const searchController = require('./../controllers/search.controller');
const shoppingCartController = require('./../controllers/shoppingCart.controller');
const productDetailController = require('./../controllers/productDetail.controller');

router.get('/', indexController.index);
router.get('/search', searchController.search);
router.get('/shopping-cart', shoppingCartController.shoppingCart);
router.get('/product-detail/:id', productDetailController.productDetail);

module.exports = router;