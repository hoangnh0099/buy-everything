const express = require('express');
const router = express.Router();

// Controller
const loginController = require('./../controllers/login.controller');
const dashboardController = require('./../controllers/dashboard.controller');
const addProductController = require('./../controllers/addProduct.controller');
const manageProductsController = require('./../controllers/manageProducts.controller');
const deleteProductController = require('./../controllers/deleteProduct.controller');
const updateProductController = require('./../controllers/updateProduct.controller');

// Middleware
const authMiddleware = require('./../middleware/auth.middleware');

// Route
router.get('/admin', loginController.admin);
router.get('/admin/dashboard', authMiddleware.authLogin, dashboardController.dashboard);
router.get('/admin/add-products', authMiddleware.authLogin, addProductController.addProduct);
router.get('/admin/manage-products', authMiddleware.authLogin, manageProductsController.manageProducts);
router.get('/admin/manage-products/delete', authMiddleware.authLogin, deleteProductController.deleteProduct);
router.get('/admin/edit-products/:id', authMiddleware.authLogin, updateProductController.editProduct);

router.post('/admin/login', loginController.login);
router.post('/admin/add-products/add', addProductController.addProducts);
router.post('/admin/edit-products/:id/edit', updateProductController.editProducts);

module.exports = router;