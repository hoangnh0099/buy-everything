const Product = require('./../models/products.models');

module.exports.editProduct = function(req, res) {
  const id = req.params.id;
  Product.findById(id, function(err, result) {
    res.render('./admin/edit-products', { 
      id: id,
      name: result.name,
      thumbnail: result.thumbnail,
      price: result.price,
      detail: result.detail
    });
  });
}

module.exports.editProducts = function(req, res) {
  const id = req.params.id;

  const newData = {
    name: req.body.productName,
    thumbnail: req.body.productThumbnail,
    price: req.body.productPrice,
    detail: req.body.productDetail
  }

  Product.findByIdAndUpdate(id, newData, function(err, result) {
    console.log(`[${new Date()}] - Data has been updated`);
    res.redirect('/admin/manage-products');
  });
}