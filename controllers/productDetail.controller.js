const Product = require('./../models/products.models');

module.exports.productDetail = function(req, res) {
  const id = req.params.id;
  Product.findById(id, function(err, result) {
    // console.log(result);
    if (err) throw err;
    res.render('./product-detail', {
      id: result.id,
      name: result.name,
      thumbnail: result.thumbnail,
      price: result.price,
      detail: result.detail
    });
  });
}