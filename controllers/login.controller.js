const AdminAccount = require('./../models/adminAccount.models');

module.exports.admin = function(req, res) {
  res.render('./admin/index');
}

module.exports.login = function (req, res, next) {
  const username = req.body.username;
  const password = req.body.password;

  AdminAccount.find().then(result => {
    if (result[0].username === username && result[0].password === password) {
      console.log(`[${new Date()}] - Loged In successful`);

      res.cookie('adminId', result[0].id, { signed: true });
      res.redirect('/admin/dashboard');
    } else {
      res.redirect('/admin');
    }
  });
}