const Product = require('./../models/products.models');

module.exports.addProduct = function(req, res) {
  res.render('./admin/add-products');
}

module.exports.addProducts = function(req, res) {
  const productName = req.body.productName;
  const productThumbnail = req.body.productThumbnail;
  const productPrice = req.body.productPrice;
  const productDetail = req.body.productDetail;

  if (productName !== null && productThumbnail !== null && productPrice !== null) {
    const product = new Product({
      name: productName, 
      thumbnail: productThumbnail, 
      price: productPrice,
      detail: productDetail
    });
    
    console.log(`${product}`);

    product.save().then(function() {
      console.log(`[${new Date()}] - Data has been saved`);
      res.redirect('/admin/manage-products');
    });
  } else {
    res.render('./admin/add-products');
  }
}