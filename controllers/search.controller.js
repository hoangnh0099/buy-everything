const Product = require('./../models/products.models');

module.exports.search = function(req, res) {
  const search = req.query.search;
  
  Product.find().then(function(product) {
    const searchResult = product.filter(product => {
      return product.name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
    });
    res.render('search-result', { 
      searchResult: searchResult
    });
  });
}