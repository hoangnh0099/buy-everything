const Product = require('./../models/products.models');

module.exports.index = function(req, res) {
  Product.find().then(function(product) {
    res.render('index', { 
      products: product.slice(start, end),
      page: page,
    });
  });

  const page = parseInt(req.query.page) || 1;
  const perPage = 8;
  const start = (page - 1) * perPage;
  const end = page * perPage;
}