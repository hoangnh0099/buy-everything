const Product = require('./../models/products.models');

module.exports.deleteProduct = function(req, res) {
  const id = req.query.id;
  console.log(`[${new Date()}] - Data has been deleted`);
  Product.findByIdAndRemove(id).then(function() {
    res.redirect('/admin/manage-products');
  });
}