const Product = require('./../models/products.models');

module.exports.manageProducts = function(req, res) {
  Product.find().then(function(product) {
    res.render('./admin/manage-products', { 
      products: product,
    });
  });
}