const express = require('express');
const userRouter = require('./routes/user.route');
const adminRouter = require('./routes/admin.route');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
require('./models/connection.models');
require('dotenv').config();

const app = express();

const PORT = process.env.PORT;

// Setup
app.set('views', './views');
app.set('view engine', 'ejs');

// Middleware
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser("jdfkjhdkjfkjfjljffjkjg"));
app.use(userRouter);
app.use(adminRouter);

app.listen(PORT, () => console.log(`[${new Date()}] - Server is running on ${PORT}`));